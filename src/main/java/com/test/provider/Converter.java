package com.test.provider;

import com.test.manager.ItemOutDTO;

public interface Converter {

    ItemOutDTO convert(RemoteResponse responseItem);
}
