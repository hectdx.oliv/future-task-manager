package com.test.provider.prov1;

import com.test.manager.ItemOutDTO;
import com.test.provider.Converter;
import com.test.provider.RemoteResponse;

public class Converter1 implements Converter {

    @Override
    public ItemOutDTO convert(RemoteResponse responseItem) {
        return new ItemOutDTO(responseItem.getId(), responseItem.getLabel());
    }
}
