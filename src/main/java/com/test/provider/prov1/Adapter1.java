package com.test.provider.prov1;

import com.test.provider.Adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Adapter1 implements Adapter {

    public String provider;

    public Adapter1(String provider) {
        this.provider = provider;
    }

    @Override
    public List getList(){

        System.out.println("Getting  list provider: " + provider);

        List<ItemProvider1> lst = new ArrayList<>() ;


        lst.add(new ItemProvider1(1,"item 1"));
        lst.add(new ItemProvider1(2,"item 2"));

        try {
            Integer i = new Random().nextInt(5);
            System.out.println(provider + ": -> wait for: " + i);
            Thread.sleep(1000 * i);
        } catch (Exception ex){

        }
        System.out.println(Thread.currentThread().getName() + "::::Returning list from provider: " + provider);

        return lst;

    }



    @Override
    public String toString() {
        return "Adapter1{" +
                "provider='" + provider + '\'' +
                '}';
    }
}
