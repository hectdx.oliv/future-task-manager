package com.test.provider.prov1;

import com.test.provider.RemoteResponse;

public class ItemProvider1 extends RemoteResponse {
    private Integer id;
    private String label;

    public ItemProvider1(Integer id, String label) {
        this.id = id;
        this.label = label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
