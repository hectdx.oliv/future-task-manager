package com.test;

import com.test.manager.Manager;
import com.test.provider.Adapter;
import com.test.provider.prov1.Converter1;
import com.test.provider.prov1.Provider1Impl;
import com.test.provider.prov1.Adapter1;
import com.test.provider.prov1.ItemProvider1;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        //directCallManager();
        asynCallManager();
    }






    private static void asynCallManager(){
        System.out.println(" ASYNC CALL ");
        long startTime = System.currentTimeMillis();

        Adapter adapter1 = new Adapter1("provider 1");
        Adapter adapter2 = new Adapter1("provider 2");



        Manager manager = new Manager();

        manager.add(new Provider1Impl(adapter1, new Converter1()));

        manager.add(new Provider1Impl(adapter2, new Converter1()));



        List<ItemProvider1> lstResponse = manager.execute();

        System.out.println("response items: " + lstResponse.size());



        long endTime = System.currentTimeMillis();

        long timeElapsed = endTime - startTime;


        System.out.println(" ---- time: " + timeElapsed + " ms ------");
    }



    private static void directCallManager(){
        System.out.println(" DIRECT CALL ");

        long startTime = System.currentTimeMillis();


        Adapter1 provider1 = new Adapter1("provider 1");
        Adapter1 provider2 = new Adapter1("provider 2");

        List<ItemProvider1> lst1 = provider1.getList();

        System.out.println("lst1: " + lst1.size());

        List<ItemProvider1> lst2 = provider2.getList();

        System.out.println("lst2: " + lst2.size());

        long endTime = System.currentTimeMillis();

        long timeElapsed = endTime - startTime;

        System.out.println(" ---- time: " + timeElapsed + " ms ------");
    }
}
