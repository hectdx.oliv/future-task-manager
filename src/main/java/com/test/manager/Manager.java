package com.test.manager;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class Manager {

    private List<FutureTask<List<ItemOutDTO>>> tasks = new ArrayList<>();

    private ExecutorService executorService;

    public List execute(){

        executorService = Executors.newFixedThreadPool(tasks.size());


        tasks.forEach(t -> executorService.submit(t));

        waitExecution();


        List<ItemOutDTO> results = new ArrayList<>();

        tasks.forEach(t -> {
            try {
                results.addAll(t.get());
            } catch (Exception ex){
                ex.printStackTrace();
            }
        });



        return results;
    }


    public void add(Template template){
        tasks.add(template.getFutureList());
    }



    private void waitExecution(){
        while (tasks.stream().anyMatch(i -> !i.isDone())) {
            //System.out.println(Thread.currentThread().getName() + ": Not all are done yet." );
        }
        System.out.println("All finished");
        executorService.shutdown();
    }
}
