package com.test.manager;

import com.test.provider.Adapter;
import com.test.provider.Converter;
import com.test.provider.RemoteResponse;
import java.util.List;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

public abstract class Template {



    private Adapter adapter;
    private Converter converter;

    public Template(Adapter adapter, Converter converter) {
        this.adapter = adapter;
        this.converter = converter;
    }

    public final FutureTask<List<ItemOutDTO>> getFutureList(){
        System.out.println(" Creating Future  ...: " + adapter);

        return new FutureTask<>(() -> {

            System.out.println(Thread.currentThread().getName() + ":: Executing callable ...: " + adapter);

            List<RemoteResponse> lstRemote = adapter.getList();

            return lstRemote.stream().map( i -> converter.convert(i)).collect(Collectors.toList());

        });
    }



}
